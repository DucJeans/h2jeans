import { LightningElement, track, api, wire } from 'lwc';
import { getRecord, getRecordNotifyChange } from 'lightning/uiRecordApi';
import { isLoading } from 'c/employeeGrid';
import EMP_NAME from '@salesforce/schema/Account.Name';
import CORP_NAME from '@salesforce/schema/Account.Employeer__r.Name';
import EMP_ID from '@salesforce/schema/Account.Legacy_Source_ID__c';
import MANG_NAME from '@salesforce/schema/Account.Manager_Name__c';
import CLIENT_CC from '@salesforce/schema/Account.Client_Cost_Center__c';
import CLIENT_BU from '@salesforce/schema/Account.Client_Business_Unit__c';
import CLIENT_US_WSTR from '@salesforce/schema/Account.Client_US_Work_Street__c';
import CLIENT_US_WCIT from '@salesforce/schema/Account.Client_US_Work_City__c';
import CLIENT_US_WSTA from '@salesforce/schema/Account.Client_US_Work_State__c';
import CLIENT_US_ZC from '@salesforce/schema/Account.Client_US_Work_Zip_Code__c';
import CLIENT_HIRED from '@salesforce/schema/Account.Client_Hired_Date__c';
import CLIENT_JOB_TITLE from '@salesforce/schema/Account.Client_Job_Title__c';
import CLIENT_JOB_DUTIES from '@salesforce/schema/Account.Client_Job_Duties__c';
import CLIENT_SALARY from '@salesforce/schema/Account.Client_Salary__c';
import CLIENT_SAL_CUR from '@salesforce/schema/Account.Client_Currency__c';
import CLIENT_US_SUITE from '@salesforce/schema/Account.Client_US_Work_Suite_Floor__c';
import CLIENT_VISA_TYPE from '@salesforce/schema/Account.I_94_Status_Good__c';
import CLIENT_VISA_EXPIRE from '@salesforce/schema/Account.I_94_Valid_To__c'

export default class EmployeeHeader extends LightningElement {
    @api recordId;
    @api isLoading;
    @track showMoreDetails = false;

    US_LOCALE = 'en-US';

    empHeaderFields = [ EMP_NAME, CORP_NAME, EMP_ID, MANG_NAME, CLIENT_CC, CLIENT_BU,
        CLIENT_US_WSTR, CLIENT_US_WCIT, CLIENT_US_WSTA, CLIENT_US_ZC, CLIENT_HIRED, CLIENT_JOB_TITLE,
        CLIENT_JOB_DUTIES, CLIENT_SALARY, CLIENT_SAL_CUR, CLIENT_US_SUITE, CLIENT_VISA_EXPIRE, CLIENT_VISA_TYPE ];
    @track empHeader = {
        displayName: '',
        corpName: '',
        empId: '',
        managerName: 'Sam Simon',
        visaType: 'H',
        visaExpiration: 'Oct 31, 2021',
        costCenter: 'IT',
        businessUnit: 'Product Development',
        workLocationLine1: '123 Main St',
        workLocationLine2: 'Dearborn, MI 48125',
        hiredDate: 'Mar 13, 2003',
        jobTitle: 'Senior Developer',
        salary: '75,000',
        salaryCurrency: 'USD',
        jobDuties: 'Manage the development process for a group of junior developers. Perform code reviews. Architect and build new software for the Salesforce platform.'
    };

    @wire(getRecord, { recordId: '$recordId', fields: '$empHeaderFields' })
    wiredRecord({ error, data }) {
        console.log('wiring');
        console.log('data:' + JSON.stringify(data));
        console.log('error:' + JSON.stringify(error));
        if (error) {
            handleError(error);
        } else if (data && data.fields) {
            this.isLoading = true;
            this.setHeaderDetails(data);
            this.isLoading = false;
        }
    }

    setHeaderDetails(data) {
        this.empHeader.displayName = data.fields.Name.value;
        this.empHeader.empId = data.fields.Legacy_Source_ID__c.value;
        this.empHeader.corpName = data.fields.Employeer__r.value.fields.Name.value;
        this.empHeader.managerName = data.fields.Manager_Name__c.value;
        this.empHeader.visaType = data.fields.I_94_Status_Good__c.value;
        this.empHeader.visaExpiration = data.fields.I_94_Valid_To__c.value;
        this.empHeader.costCenter = data.fields.Client_Cost_Center__c.value;
        this.empHeader.businessUnit = data.fields.Client_Business_Unit__c.value;
        this.empHeader.workLocationLine1 = data.fields.Client_US_Work_Street__c.value;
        this.empHeader.workLocationLine2 = ((data.fields.Client_US_Work_City__c.value ? data.fields.Client_US_Work_City__c.value : '') + ', ' +
                                            (data.fields.Client_US_Work_State__c.value ? data.fields.Client_US_Work_State__c.value : '') + ' ' +
                                            (data.fields.Client_US_Work_Zip_Code__c.value ? data.fields.Client_US_Work_Zip_Code__c.value : '') + ' ' +
                                            (data.fields.Client_US_Work_Suite_Floor__c.value ? data.fields.Client_US_Work_Suite_Floor__c.value : ''));
        this.empHeader.hiredDate = data.fields.Client_Hired_Date__c.value;
        this.empHeader.jobTitle = data.fields.Client_Job_Title__c.value;
        this.empHeader.jobDuties = data.fields.Client_Job_Duties__c.value;
        this.empHeader.salary = ('$' + (data.fields.Client_Salary__c.value.toLocaleString(this.US_LOCALE)));
        this.empHeader.salaryCurrency = data.fields.Client_Currency__c.value;
    }

    handleLessDetails(event) {
        this.showMoreDetails = false;
    }

    handleMoreDetails(event) {
        this.showMoreDetails = true;
    }
 
    handleError(error) {
        let message = 'Unknown error';
        try {
            console.error('Error:', JSON.stringify(error));
            if (Array.isArray(error.body)) {
                message = error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                message = error.body.message;
            }
        } catch (loggingError) {
            console.error('Logging Error:', JSON.stringify(loggingError));
        }
        this.showError(message);
    }

    showError(message) {
        this.showToast('Error:', message, 'error');
    }

    showSuccess(message) {
        this.showToast('Success:', message, 'success');
    }

    showToast(title, message, variant) {
        this.dispatchEvent(
            new ShowToastEvent({
                title: title,
                message: message,
                variant: variant,
            }),
        );
    }
}