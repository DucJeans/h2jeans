import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getEmployees from '@salesforce/apex/auroraLibrary.getEmployees';

const TABLE_TITLE = 'Employees';

export default class EmployeeGrid extends LightningElement {
    @api view;
    userId;
    empDataMaster = [];
    tableTitle = TABLE_TITLE;
    @track isLoading = false;

    @track empData;
    @track empColumns = [];
    empSortDirection = 'asc';
    empSortedBy;
    @track showEmpDataTable = true;
	
    @track caseData;
    @track caseColumns = [];
    caseSortDirection = 'asc';
    caseSortedBy;

    defaultSortDirection = 'asc';
    

    showCaseModal = false;
    showColumnModal = false;
    showCaseStepModal = false;

    connectedCallback() {
        this.initComponent();
    }

    initComponent() {
        if (this.isEmployeesView()) {
            this.getEmployeeList();
        } 
    }

    getEmployeeList() {
        /*let data = [
            { id: "1", empId: "EMP435464", actionUrl: "/#", empIdUrl: "/aurora/s/account/00119000014ZU7tAAG/community-user", fileNum: "1234", jobTitle: "Tester", empName: "Community User", corporation: "Test Corp",  citizenship: "India",  caseStatus: "Approved", statusExp: "10/1/2021", immigrationStatus: "H1-B" },
            { id: "2", empId: "EMP76733", actionUrl: "/#", empIdUrl: "/aurora/s/account/00119000014b4pzAAA/employee-tester", fileNum: "5678", jobTitle: "Developer", empName: "Employee Tester", corporation: "Test Corp",  citizenship: "Czech Republic", caseStatus: "Open", statusExp: "10/31/2021", immigrationStatus: "H1-B" }
        ];*/
        this.isLoading = true;
        getEmployees({ sId: '00119000014b3AHAAY' })
            .then((result) => {
                console.log('Data:' + JSON.stringify(result));
                let data = result.tableData;
                this.isLoading = false;
                this.empDataMaster = data;
                this.setEmpTableDisplay(data);
            })
            .catch((error) => {
                this.isLoading = false;
                this.handleTableDataError(error);
            });
        
        /*console.info('before Loading');
        //this.isLoading = true;
        console.info('getEmployees');
        //create a fake controller method
        let getEmployees = new Promise(function(){ 
            // This will be a controller
            return {employees: [
                { empName : "Test McTest", status: "In Progress", nextMilestone: "Intake", milestoneDate: "10/1/2021" }
            ]};
        })
        // 
        
        // only get ipd data filtered for agent commissions (First Year)
        getEmployees({ userId: this.userId })
        .then((result) => {
            console.info('result:' + JSON.stringify(result));
            //this.isLoading = false;
            this.setEmpTableDisplay(result.employees);
        })
        .catch((error) => {
            //this.isLoading = false;
            this.handleTableDataError(error);
        }); 
        //this.setEmpTableDisplay(this.getEmployees());
        */
    }
/*
    getEmployees() {
        // This will be a controller
        return {employees: [
            { empName : "Test McTest", status: "In Progress", nextMilestone: "Intake", milestoneDate: "10/1/2021" }
        ]};
    }
*/

    getCaseData() {
        let data = [
            { id: "1", caseId: "CS435464", caseType: "ARE - Business Visa", caseIdUrl: "/aurora/s/matter/a0L5C000002k8BiUAI", country: "UAE", dateOpened: "Oct 1, 2021", validTo: "", receiptNumber: "R34534545",  status: "Open" },
            { id: "2", caseId: "CS435533", caseType: "ARE - Business Visa", caseIdUrl: "/aurora/s/matter/a0L5C000002k8BiUAI", country: "UAE", dateOpened: "Oct 1, 2021", validTo: "Oct 31, 2021", receiptNumber: "R68678769", status: "Open" }
        ];

        //https://eig--partial.livepreview.salesforce-communities.com/aurora/s/matter/a0L5C000002k8BiUAI/mat2109272833
        this.setCaseTableDisplay(data);
    }

    setEmpTableDisplay(data) {
        console.info('set table')
        // set up table
        this.empData = data;
        this.empColumns = [
            { type: 'button', typeAttributes: {  
                label: 'View Cases',  
                name: 'View',  
                title: 'View Cases',  
                disabled: false,  
                value: 'view', 
                iconPosition: 'left'  
            }, fixedWidth: 120},
            { label: 'Employee ID', fieldName: 'empId', sortable: true, wrapText: true},
            { label: 'File Number', fieldName: 'fileNum', sortable: true, wrapText: true},
            { label: 'Job Title', fieldName: 'jobTitle', sortable: true, wrapText: true},
            { label: 'Corporation', fieldName: 'corporation', sortable: true, wrapText: true},
            { label: 'Citizenship', fieldName: 'citizenship', sortable: true, wrapText: true},
            
            { type: 'url', fieldName: 'empIdUrl', label: 'Full Name', wrapText: true,
                        typeAttributes: { label: { fieldName: 'empName' }},     
                    },
            { label: 'Case Status', type: 'customStatus', fieldName: 'caseStatus', sortable: true, wrapText: true},
            { label: 'Status Expiration', fieldName: 'statusExp', sortable: true, wrapText: true},
            { label: 'Programs', type:'customEmpBadges', fieldName: 'immigrationStatus', sortable: false, fixedWidth: 160,
                typeAttributes: {
                    travel: {fieldName: 'travel'},
                    work: {fieldName: 'work'},
                    residency: {fieldName: 'residency'},
                    executive: {fieldName: 'executive'}
                }
            }
        ];
/* 

            */
        // if no data display message
        if (this.empData && this.empData.length > 0) {
            this.showEmpDataTable = true;
        } 
    }

    setCaseTableDisplay(data) {
        console.info('set case table')
        // set up table
        this.caseData = data;
        this.caseColumns = [
            { type: 'button', typeAttributes: {  
                label: 'View Steps',  
                name: 'View',  
                title: 'View Steps',  
                disabled: false,  
                value: 'view',  
                iconPosition: 'left'  
            }},
            { label: 'Case ID', fieldName: 'caseId', sortable: true, wrapText: true},
            { type: 'url', fieldName: 'caseIdUrl', label: 'Case Type', wrapText: true,
                        typeAttributes: { label: { fieldName: 'caseType' }},     
                    },
            { label: 'Country', fieldName: 'country', sortable: true, wrapText: true},
            { label: 'Date Opened', fieldName: 'dateOpened', sortable: true, wrapText: true},
            { label: 'Valid To', fieldName: 'validTo', sortable: true, wrapText: true},
            { label: 'Receipt Number', fieldName: 'receiptNumber', sortable: true, wrapText: true},
            { label: 'Status', fieldName: 'status', sortable: true, wrapText: true}
        ];

        // if no data display message
        if (this.caseData && this.caseData.length > 0) {
        } 
    }

    sortBy(field, reverse, primer) {
        const key = primer
            ? function (x) {
                  return primer(x[field]);
              }
            : function (x) {
                  return x[field];
              };

        return function (a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    }

    handleEmpGridSort(event) {
        const { fieldName: empSortedBy, sortDirection } = event.detail;
        const cloneData = [...this.empData];

        cloneData.sort(this.sortBy(empSortedBy, sortDirection === 'asc' ? 1 : -1));
        this.empData = cloneData;
        this.empSortDirection = sortDirection;
        this.empSortedBy = empSortedBy;
    }

    handleCaseGridSort(event) {
        const { fieldName: caseSortedBy, sortDirection } = event.detail;
        const cloneData = [...this.caseData];

        cloneData.sort(this.sortBy(caseSortedBy, sortDirection === 'asc' ? 1 : -1));
        this.caseData = cloneData;
        this.caseSortDirection = sortDirection;
        this.caseSortedBy = caseSortedBy;
    }

    handleEmpGridSearch(event) {
        let filterString = event.target.value
        let cloneData = [...this.empDataMaster];
        if (filterString) {
            filterString = filterString.toLowerCase();
            cloneData = cloneData.filter(d => {
                return ((d.empId && d.empId.toLowerCase().includes(filterString))
                || (d.empName && d.empName.toLowerCase().includes(filterString))
                || (d.fileNum && d.fileNum.toLowerCase().includes(filterString))
                || (d.jobTitle && d.jobTitle.toLowerCase().includes(filterString))
                || (d.corporation && d.corporation.toLowerCase().includes(filterString))
                || (d.citizenship && d.citizenship.toLowerCase().includes(filterString))
                || (d.caseStatus && d.caseStatus.toLowerCase().includes(filterString))
                || (d.immigrationStatus && d.immigrationStatus.toLowerCase().includes(filterString))
                )
              });
        }

        if (this.sortBy) {
            cloneData.sort(this.sortBy(this.empSortedBy, this.empSortDirection === 'asc' ? 1 : -1));
        }
        this.empData = cloneData;
    }

    handleEmpRowAction( event ) {  
        console.info('got actions');
        const recId =  event.detail.row.Id;  
        const actionName = event.detail.action.name;  
        if ( actionName === 'View') {  
           this.openCaseModal();
        }          
  
    }

    handleCaseRowAction( event ) {  
        console.info('got actions');
        const recId =  event.detail.row.Id;  
        const actionName = event.detail.action.name;  
        if ( actionName === 'View') {  
           this.openCaseStepModal();
        }          
  
    }

    openCaseStepModal() {
        this.showCaseStepModal = true;
        this.showCaseModal = false;
    }

    handleCloseCaseStepModal() {
        this.showCaseStepModal = false;
        this.showCaseModal = true;
    }

    openColumnModal() {
        this.showColumnModal = true;
    }

    handleCloseColumnModal() {
        this.showColumnModal = false;
    }

    openCaseModal() {
        this.getCaseData();
        this.showCaseModal = true;
    }

    handleCloseCaseModal() {
        this.showCaseModal = false;
    }

    handleTableDataError(error) {
        console.error('ERROR IN TABLE DATA:' + JSON.stringify(error));
        this.handleError(error);
        this.showEmpDataTable = false;
    }

    handleError(error) {
        console.error('Error:', JSON.stringify(error));
        let message = 'Unknown error';
        if (Array.isArray(error.body)) {
            message = error.body.map(e => e.message).join(', ');
        } else if (typeof error.body.message === 'string') {
            message = error.body.message;
        }
        this.showError(message);
    }

    showError(message) {
        this.showToast('Error:', message, 'error');
    }

    showSuccess(message) {
        this.showToast('Success:', message, 'success');
    }

    showToast(title, message, variant) {
        this.dispatchEvent(
            new ShowToastEvent({
                title: title,
                message: message,
                variant: variant,
            }),
        );
    }

    isEmployeesView() {
        return this.view && this.view === 'employeetable';
    }
}